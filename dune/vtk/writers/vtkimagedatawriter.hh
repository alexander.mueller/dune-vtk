#pragma once

#ifndef DUNE_VTK_DISABLE_DEPRECATION_WARNING
#warning "File vtkimagedatawriter.hh and VtkImageDataWriter are deprecated. Use imagedatawriter.hh and Vtk::ImageDataWriter instead."
#endif

#include <dune/vtk/writers/imagedatawriter.hh>

namespace Dune
{
  template <class GridView, class DataCollector = Vtk::StructuredDataCollector<GridView>>
  class [[deprecated("Use Vtk::ImageDataWriter instead.")]] VtkImageDataWriter
    : public Vtk::ImageDataWriter<GridView,DataCollector>
  {
    using Base = Vtk::ImageDataWriter<GridView,DataCollector>;
  public:
    using Base::Base;
  };

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  // deduction guides
  template <class GridView, class... Args,
    Vtk::IsGridView<GridView> = true>
  VtkImageDataWriter(GridView, Args...)
    -> VtkImageDataWriter<GridView, Vtk::StructuredDataCollector<GridView>>;

  template <class DataCollector, class... Args,
    Vtk::IsDataCollector<DataCollector> = true>
  VtkImageDataWriter(DataCollector&, Args...)
    -> VtkImageDataWriter<typename DataCollector::GridView, DataCollector>;

  template <class DataCollector, class... Args,
    Vtk::IsDataCollector<DataCollector> = true>
  VtkImageDataWriter(std::shared_ptr<DataCollector>, Args...)
    -> VtkImageDataWriter<typename DataCollector::GridView, DataCollector>;
#pragma GCC diagnostic pop

} // end namespace Dune
