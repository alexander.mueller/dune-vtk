// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#if HAVE_CONFIG_H
#include "config.h" // autoconf defines, needed by the dune headers
#endif

#include <dune/vtk/vtkreader.hh>
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/writers/imagedatawriter.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#if HAVE_DUNE_UGGRID
  using GridType = Dune::UGGrid<2>;
#else
  using GridType = Dune::YaspGrid<2>;
#endif

int main (int argc, char** argv)
{
  using namespace Dune;
  MPIHelper::instance(argc, argv);

  auto grid = StructuredGridFactory<GridType>::createCubeGrid({0.0,0.0}, {1.0,2.0}, {2u,4u});
  auto gridView = grid->leafGridView();

  // 1. construct writer from gridView
  Vtk::UnstructuredGridWriter writer1(gridView);

  // 2. construct writer from datacollector
  Vtk::ContinuousDataCollector dataCollector1(gridView);
  Vtk::UnstructuredGridWriter writer2(dataCollector1);
  Vtk::UnstructuredGridWriter writer3(stackobject_to_shared_ptr(dataCollector1));

  auto writer4 = Vtk::UnstructuredGridWriter(Vtk::ContinuousDataCollector(gridView));

  if constexpr(std::is_same_v<GridType, Dune::YaspGrid<2>>)
    auto writer5 = Vtk::ImageDataWriter(Vtk::ContinuousDataCollector(gridView));

  // 3. construct a default VtkWriter
  Vtk::VtkWriter writer6(gridView);

  // 4. construct reader from grid-factory
  GridFactory<GridType> factory;
  Vtk::VtkReader reader1(factory);

  // 5. construct reader from grid-creator
  Vtk::ContinuousGridCreator creator(factory);
  Vtk::VtkReader reader2(creator);
}
